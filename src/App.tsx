import { FC } from 'react';
import { useGetJobsQuery } from 'services/BaseApi';
import Loader from 'components/Loader/Loader';

const App: FC = () => {
  const { isLoading, data } = useGetJobsQuery();

  if (isLoading) {
    return <Loader show heightPx={740} />;
  }

  return <div>{JSON.stringify(data)}</div>;
};

export default App;
