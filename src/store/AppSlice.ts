import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ELanguages } from 'types/common';
import type { TRootState } from './index';

interface IAppState {
  isLoading: boolean;
  language: ELanguages;
}

const initialState: IAppState = {
  isLoading: false,
  language: ELanguages.EN_GB,
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setIsLoading: (state, action: PayloadAction<boolean>) => {
      return {
        ...state,
        isLoading: action.payload,
      };
    },
    setLanguage: (state, action: PayloadAction<ELanguages>) => {
      return {
        ...state,
        language: action.payload,
      };
    },
  },
});

export const { setIsLoading, setLanguage } = appSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const isLoading = (state: TRootState): boolean => state.app.isLoading;
export const getLanguage = (state: TRootState): ELanguages => state.app.language;

export default appSlice.reducer;
