import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { ELanguages } from 'types/common';
import type { IJobsResponse } from './types';

// Define a service using a base URL and expected endpoints
export const baseApi = createApi({
  reducerPath: 'baseApi',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL || '',
  }),
  tagTypes: ['Jobs'],
  endpoints: (builder) => ({
    getTranslations: builder.query<object, ELanguages>({
      query: (language) => {
        return `translations/${language}`;
      },
    }),
    getJobs: builder.query<IJobsResponse[], void>({
      query: () => 'jobs',
      providesTags: (response) =>
        response ? response.map(({ id }) => ({ type: 'Jobs', id })) : ['Jobs'],
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useLazyGetTranslationsQuery, useGetJobsQuery } = baseApi;
