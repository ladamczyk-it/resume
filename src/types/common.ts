export interface IDictionary<T> {
  [key: string]: T;
}

export interface ISelectOption {
  value: number;
  label: string;
}

export enum ELanguages {
  PL = 'pl',
  EN_GB = 'en_GB',
}
