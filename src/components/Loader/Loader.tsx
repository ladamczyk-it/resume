import { CSSProperties, FC } from 'react';
import { Col, Row, Spinner } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

export interface ILoaderProps {
  show: boolean;
  heightPx?: number;
  marginTopPx?: number;
  zIndex?: number;
  onlySpinner?: boolean;
}

const Loader: FC<ILoaderProps> = ({ show, heightPx, marginTopPx, zIndex, onlySpinner }) => {
  const { t } = useTranslation();

  if (!show) {
    return null;
  }

  const spinner = (
    <div className="text-center">
      {t('loading')} <Spinner animation="border" size="sm" />
    </div>
  );

  if (onlySpinner) {
    return spinner;
  }

  const style: CSSProperties = {};

  if (heightPx) {
    style.height = `${heightPx}px`;
  }

  if (heightPx && zIndex) {
    style.height = `${heightPx}px`;
    style.width = '99%';
    style.zIndex = zIndex;
    style.position = 'absolute';
    style.background = '#fff';
  }

  if (heightPx && marginTopPx && zIndex) {
    style.height = `${heightPx - marginTopPx}px`;
    style.marginTop = `${marginTopPx}px`;
  }

  return (
    <Row className="justify-content-center align-items-center" style={style}>
      <Col md="auto">{spinner}</Col>
    </Row>
  );
};

export default Loader;
