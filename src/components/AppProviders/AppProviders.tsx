import { FC, ReactNode } from 'react';
import { Provider } from 'react-redux';
import store from 'store';
import { TranslationsProviders } from './providers';

interface IAppProvidersProps {
  children: ReactNode;
}

const AppProviders: FC<IAppProvidersProps> = ({ children }) => {
  return (
    <Provider store={store}>
      <TranslationsProviders>{children}</TranslationsProviders>
    </Provider>
  );
};

export default AppProviders;
