import { FC, useState, useEffect, ReactNode } from 'react';
import i18next from 'i18next';
import { I18nextProvider, initReactI18next } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';
import { useLazyGetTranslationsQuery } from 'services/BaseApi';
import { useAppSelector } from 'store/hooks';
import { ELanguages } from 'types/common';
import { getLanguage } from 'store/AppSlice';

i18next.use(initReactI18next).init({
  lng: ELanguages.EN_GB,
  supportedLngs: Object.values(ELanguages),
});

interface ITranslationsProvidersProps {
  children: ReactNode;
}

const TranslationsProviders: FC<ITranslationsProvidersProps> = ({ children }) => {
  const NS = 'translations';
  const language = useAppSelector(getLanguage);
  const [trigger] = useLazyGetTranslationsQuery();
  const [translationsKey, setTranslationsKey] = useState<string>(uuidv4());

  useEffect(() => {
    trigger(language).then(({ data: responseData }) => {
      i18next.addResourceBundle(language, NS, responseData, true, true);

      setTranslationsKey(uuidv4());
    });
  }, [trigger, language]);

  return (
    <I18nextProvider key={translationsKey} i18n={i18next} defaultNS={NS}>
      {i18next.getResourceBundle(language, NS) && children}
    </I18nextProvider>
  );
};

export default TranslationsProviders;
