try {
  require('dotenv').config({ path: './.env.local' });
} catch (e) {
  // dont do anything this is just for initial deploy
}

const PRODUCTION_USERNAME = process.env.PRODUCTION_USERNAME || '';
const PRODUCTION_HOST = process.env.PRODUCTION_HOST || '';
const PRODUCTION_REPO_ADDRESS = process.env.PRODUCTION_REPO_ADDRESS || '';
const PRODUCTION_PATH_ON_SERVER = process.env.PRODUCTION_PATH_ON_SERVER || '';

module.exports = {
  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: PRODUCTION_USERNAME,
      host: PRODUCTION_HOST,
      ref: 'origin/master',
      repo: PRODUCTION_REPO_ADDRESS,
      path: PRODUCTION_PATH_ON_SERVER,
      'post-deploy': 'npm ci && npm run build',
    },
  },
};
