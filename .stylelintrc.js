const packageJson = require('./package.json');

module.exports = {
  extends: ['stylelint-config-standard-scss', 'stylelint-prettier/recommended'],

  plugins: [
    'stylelint-file-max-lines',

    'stylelint-high-performance-animation',

    'stylelint-no-unsupported-browser-features',

    'stylelint-order',
  ],

  rules: {
    'plugin/file-max-lines': 600,

    'plugin/no-low-performance-animation-properties': true,

    'plugin/no-unsupported-browser-features': [
      true,

      {
        browsers: packageJson.browserslist,

        ignorePartialSupport: true,
      },
    ],

    // config from https://github.com/simonsmith/stylelint-selector-bem-pattern/issues/23#issuecomment-1405100654

    'selector-class-pattern': [
      // added |^pb- to turn off all PB overrides selectors errors since we don't have control over that

      '^[a-z]([-]?[a-z0-9]+)*(__[a-z0-9]([-]?[a-z0-9]+)*)?(--[a-z0-9]([-]?[a-z0-9]+)*)?$|^pb-',

      {
        resolveNestedSelectors: true,

        message: function expected(selectorValue) {
          return `Expected class selector "${selectorValue}" to match BEM CSS pattern https://en.bem.info/methodology/css`;
        },
      },
    ],
  },
};
