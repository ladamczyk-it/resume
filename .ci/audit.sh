#!/bin/bash

LINT_MESSAGE=$(npm audit --summary --prod --no-progress --json)
echo $LINT_MESSAGE

case "$LINT_MESSAGE" in
  *'"high":0,"critical":0}'*)
    exit 0
    ;;
  *)
    echo 'Check dependencies!';
    exit 1
    ;;
esac
