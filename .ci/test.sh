#!/bin/bash

npm run test:coverage
RESULT=$?

if [ ! $RESULT -eq 0 ]; then
  echo "Some tests failed. Please fix them!"
  exit $RESULT
fi

echo "All tests passes!"