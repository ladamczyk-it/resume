#!/bin/bash

LINT_MESSAGE=$(npm run lint)
echo $LINT_MESSAGE

case "$LINT_MESSAGE" in
  *"No lint errors found!"*)
    exit 0
    ;;
  *)
    echo 'Fix lint errors before commit!';
    exit 1
    ;;
esac
