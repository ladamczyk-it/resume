const jsonServer = require('json-server');
const server = jsonServer.create();
const middlewares = jsonServer.defaults();

const pl = require('./response/i18n/pl.json');
const en_GB = require('./response/i18n/en_GB.json');
const jobsResponse = require('./response/jobs.json');

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.get('/echo', (req, res) => {
  res.jsonp(req.query);
});

server.get('/translations/:language', (req, res) => {
  const { language } = req.params;

  switch (language) {
    case 'pl':
      return res.jsonp(pl_PL);
    case 'en_GB':
      return res.jsonp(en_GB);
    default:
      return res.statusCode(404);
  }
});

server.get('/jobs', (_, res) => {
  res.jsonp(jobsResponse);
});

const port = 19800;

server.listen(port, () => {
  console.log('JSON Server is running ' + port);
});
