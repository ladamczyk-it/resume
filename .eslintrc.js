const commonRules = {
  'import/prefer-default-export': 0,
  'react/jsx-props-no-spreading': 0,
  'class-methods-use-this': 0,
  'react/function-component-definition': 0,
  'no-restricted-exports': 0,
  'react/require-default-props': 0,
};

const developmentRules = {
  '@typescript-eslint/naming-convention': [
    'warn',
    {
      selector: 'interface',
      prefix: ['I'],
      format: ['PascalCase'],
    },
    {
      selector: 'typeAlias',
      prefix: ['T'],
      format: ['PascalCase'],
    },
    {
      selector: 'enum',
      prefix: ['E'],
      format: ['PascalCase'],
    },
    {
      selector: 'enumMember',
      format: ['UPPER_CASE'],
    },
    {
      selector: 'class',
      format: ['PascalCase'],
    },
    {
      selector: ['classProperty', 'classMethod', 'method', 'function'],
      format: ['camelCase'],
    },
    {
      selector: 'parameter',
      leadingUnderscore: 'allow',
      format: ['camelCase'],
    },
    {
      selector: 'variable',
      format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
    },
  ],
  '@typescript-eslint/explicit-module-boundary-types': 'warn',
  '@typescript-eslint/no-explicit-any': 'warn',
  '@typescript-eslint/no-unused-vars': ['warn', { args: 'after-used' }],
  'import/no-cycle': 'warn',
  'import/order': 'warn',
  'jsx-a11y/anchor-is-valid': 'warn',
  'prettier/prettier': 'warn',
  'react/jsx-filename-extension': ['warn', { extensions: ['.jsx', '.tsx'], allow: 'as-needed' }],
  'react-hooks/rules-of-hooks': 'warn',
  'react-hooks/exhaustive-deps': 'warn',
  'consistent-return': 'warn',
  curly: ['warn', 'all'],
  eqeqeq: 'warn',
  'max-lines': ['warn', { max: 500, skipBlankLines: true, skipComments: true }],
  'max-lines-per-function': ['warn', { max: 250, skipBlankLines: true, skipComments: true }],
  'no-console': ['warn', { allow: ['error', 'warn'] }],
  'no-debugger': 'warn',
  'no-restricted-imports': [
    'warn',
    {
      paths: [
        {
          name: 'lodash',
          message: 'Please use separate import for all lodash functions instead.',
        },
      ],
      patterns: [
        {
          group: ['../../*'],
          message: 'Agreed maximum one level back for relative imports.',
        },
      ],
    },
  ],
  'no-useless-return': 'warn',
  'padding-line-between-statements': [
    'warn',
    { blankLine: 'always', prev: '*', next: 'return' },
    { blankLine: 'always', prev: 'block-like', next: '*' },
  ],
};

const strictRules = JSON.parse(
  JSON.stringify(developmentRules)
    .replace(new RegExp('"warn"', 'g'), '"error"') // for all rules
    .replace('{"allow":["error","error"]}', '{"allow":["error","warn"]}') // for 'no-console' only
);

module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'import', 'sonarjs', 'jest', 'jest-formatting', 'prettier'],
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'react-app',
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'airbnb/hooks',
    'airbnb-typescript',
    'plugin:import/typescript',
    'plugin:sonarjs/recommended',
    'plugin:compat/recommended',
    'plugin:jest-formatting/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  parserOptions: {
    project: './tsconfig.json',
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  globals: {
    React: true,
    JSX: true,
    window: true,
    document: true,
  },
  settings: {
    'import/resolver': {
      typescript: {},
    },
    react: {
      pragma: 'React',
      version: 'detect',
    },
  },
  rules:
    process.env.NODE_ENV === 'strictRules'
      ? { ...commonRules, ...strictRules }
      : { ...commonRules, ...developmentRules },
  overrides: [
    {
      files: ['./**/*.spec.ts', './**/*.spec.tsx', './typings/**/*'],
      rules: {
        'import/no-extraneous-dependencies': 0,
        'sonarjs/no-duplicate-string': 0,
        'sonarjs/cognitive-complexity': 0,
        'sonarjs/no-identical-functions': 0,
        'no-template-curly-in-string': 0,
        '@typescript-eslint/no-explicit-any': 0,
        'max-lines-per-function': 0,
        'no-console': 0,
        'react/jsx-no-constructed-context-values': 0,
      },
    },
  ],
};
