const path = require('path');
const merge = require('lodash/merge');
const { alias, aliasJest, configPaths } = require('react-app-rewire-alias');

const aliasMap = configPaths('./tsconfig.paths.json'); // or jsconfig.paths.json

module.exports = function override(config, _env) {
  const modifiedConfig = alias(aliasMap)(config);

  const rules = modifiedConfig.module.rules.find((ruleType) => ruleType.oneOf);

  const sassRules = rules.oneOf.filter((rule) => {
    return rule.test && rule.test.toString().includes('sass');
  });

  sassRules.some((rule) => {
    const sassConfig = rule.use.find((i) => i.loader && i.loader.includes('sass-loader'));

    if (sassConfig) {
      const { options } = sassConfig;

      options.additionalData = '@import "styles/core";';

      options.sassOptions = merge({}, options.sassOptions, {
        includePaths: [path.resolve(process.cwd(), './src')],
      });
    }

    return sassConfig;
  });

  return modifiedConfig;
};

module.exports.jest = aliasJest(aliasMap);
